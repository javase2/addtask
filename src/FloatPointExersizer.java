
import java.util.Scanner;

public class FloatPointExersizer {

	public static void main(String[] args) {
		
		float f;
		
		System.out.println("Введите значение:");
				
		try(var input = new Scanner(System.in)) {		
			f = input.nextFloat();		
		} catch(java.util.InputMismatchException e) {
			System.out.println("Введено неправильное значение");
			return;
		}
		System.out.println("|31  |27  |23  |19  |15  |11  |7   |3 |0   |Hex Value   |Dec Value");				
		
		System.out.print(alignedBitString(Integer.toBinaryString(Float.floatToRawIntBits(f))));
		System.out.println("    " + Float.toHexString(f) + "\t" + f);
		
		System.out.println("||        ||--------------------------|Mantissa");
		System.out.println("||--------|Exponent");
		System.out.println("|Sign");
		
	}
	
	static String alignedBitString(String str) {
		StringBuilder sb = new StringBuilder(str);
		while(sb.length() < 32) {
			sb.insert(0, '0');
		}
		for(int i = 4; i < 35; i+=5) {
			sb.insert(i, ' ');
		}
		return sb.toString();
	}
}
