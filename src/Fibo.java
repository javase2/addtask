
public class Fibo {

	public static void main(String[] args) {

        // 1 1 2 3 4 8 13 21 34 55 89 и т.д.

        int a = 1;
        int b = 1;
        int c;
        System.out.print (a + " " +b+ " ");
        for ( int i = 3; i <= 11; i++){
            c = a+b;
            System.out.print(c+ " ");
            a=b;
            b=c;
        }
        System.out.println();
    }
}
