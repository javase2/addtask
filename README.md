# addtask

# **# Условие к дополнительному заданию "Рефакторинг":**

Дан класс Converter, предоставляющий функционал пересчета между различными единицами измерения. 

В его текущей версии реализована конвертация из градисов Цельсия в градусы Фаренгейта и обратно.

Необходимо выполнить рефакторинг кода в соответствии с шаблоном "Стратегия" (см. материалы последней лекции) и добавить пересчет из километров в мили и обратно.

Любые другие улучшения кода приветствуются.

(См. вложенный файл: Converter.java)**




#Для запуска проекта скопируйте указанный ниже код
#Вставьте с ПОЛНОЙ заменой в файл tasks.json
#Расположение файла tasks.json: Explorer => первая папка ".theia" => tasks.json

{
    "tasks": [
        {
            "type": "che",
            "label": "FloatPointExersizer build and run",
            "command": "javac -sourcepath /projects/addtask/src -d /projects/addtask/bin /projects/addtask/src/*.java && java -classpath /projects/addtask/bin FloatPointExersizer",
            "target": {
                "workingDir": "${CHE_PROJECTS_ROOT}/addtask/src",
                "component": "maven"
            }
        }
    ]
}

тест